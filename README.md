WebTools
============================
Tool for creating and managing Content Managing Systems

Changelog
----------------------------
Version 0.1:

Usage
----------------------------
### Preconditions: ###

1. Set mysql password in helpers.py
2. On Windows: make sure you have path to mysql.exe folder in system path

### Examples ###
Example 1 - Display help:
```bash
python webtools_cmd.py --help
```

Example 2 - install new Wordpress site:
```bash
python webtools_cmd.py --action="install" --directory="/var/www/sites" --folder_name="www.example.com" --site_title="My Blog" --admin_username="webmaster" --admin_password="MyPassword1234" --admin_email="name@domain.com" --blog_public="1"
```

Example 3 - Get DB info for an installation:
```bash
python webtools_cmd.py --action="getSingle" --directory="/var/www/sites" --folder_name="www.example.com"
```

Example 4 - Change status. Ie add/remove from /etc/apache2/sites-enabled:
```bash
python webtools_cmd.py --action="enableSite" --enable="true" --folder_name="www.example.com"
```
