"""

  Title        : Orbit WEB Access command line web tool
  Description  : Tools for managing cms installations from command line
  Author       : okdios
  Date         : 1/11-2013
  Version      : 0.1
  Python_ver.  : 2.7
  Notes        :

"""

import argparse
from webtools import WebTools


class WebToolsCmd:

    def __init__(self):
        self.configure_arguments()
        self.handle_arguments()

    def configure_arguments(self):
        parser = argparse.ArgumentParser()
        parser.add_argument(
            '-d', '--directory',
            help="Base Directory for installations")
        parser.add_argument(
            '-c', '--cms',
            help="Select CMS. Default is WORDPRESS")
        parser.add_argument(
            '-f', '--folder_name',
            help="Name of new installation")
        parser.add_argument(
            '-s', '--site_title',
            help="Site/blog title")
        parser.add_argument(
            '-u', '--admin_username',
            help="Admin username")
        parser.add_argument(
            '-p', '--admin_password',
            help="Admin password")
        parser.add_argument(
            '-e', '--admin_email',
            help="Admin email address")
        parser.add_argument(
            '-b', '--blog_public',
            help="Should the blog be public right away? 0 or 1")
        parser.add_argument(
            '-k', '--enable',
            help="Boolean, true/false. Generic option")
        parser.add_argument(
            '-m', '--action',
            help="action to call")
        parser.add_argument(
            '-v', '--debug',
            help="Enable additional debug output. True/False. Default: False")
        parser.add_argument(
            '-j', '--json',
            help="Enable output as json. True/False. Default is True")
        args = parser.parse_args()
        self.args = args

    def handle_arguments(self):
        if self.args.action is None:
            print "No action is defined! Nothing to do. Set --action=<action>"
            return
        web = WebTools()
        # First set all options
        if self.args.directory:
            web.setDir(self.args.directory)
        if self.args.cms:
            web.setCMS(self.args.cms)
        if self.args.folder_name:
            web.setFolderName(self.args.folder_name)
        if self.args.debug:
            web.enableDebug(self.args.debug)
        if self.args.json:
            web.enableJson(self.args.json)
        if self.args.site_title:
            web.setSiteTitle(self.args.site_title)
        if self.args.admin_username and self.args.admin_password and self.args.admin_email:
            web.setSiteAdmin(self.args.admin_username,
                             self.args.admin_password,
                             self.args.admin_email)
        if self.args.blog_public:
            web.setBlogPublic(self.args.blog_public)

        # Finally call the action
        if self.args.action == "install":
            web.install()
        elif self.args.action == "getAllInstallations":
            print web.getAllInstallations()
        elif self.args.action == "getSingle":
            if self.args.folder_name:
                print web.getSingle(self.args.folder_name)
            else:
                print "Folder name is not defined! \
Set it with --folder_name='<folder_name>'"
        elif self.args.action == "enableSite":
            if not self.args.enable:
                print "enable is not set! Set it with --enable='<true/false>'"
            elif not self.args.folder_name:
                print "Folder name is not defined! \
Set it with --folder_name='<folder_name>'"
            else:
                print web.enableSite(self.args.folder_name, self.args.enable)

        elif self.args.action == "removeInstallation":
            if not self.args.folder_name:
                print "Folder name is not defined! \
Set it with --folder_name='<folder_name>'"
            else:
                print web.removeInstallation(self.args.folder_name)

        else:
            print "Unknown action '" + self.args.action + "'"


if __name__ == '__main__':
    web = WebToolsCmd()
