"""

  Title        : Orbit WEB Access web tool
  Description  : Tools for managing cms installations on a webserver
  Author       : okdios
  Date         : 29/10-2013
  Version      : 0.1
  Python_ver.  : 2.7
  Notes        :

"""

import re
import json
import tarfile
from os import listdir, walk, geteuid, remove
from os import name as os_name
from os.path import isdir, join, exists
from os import system as system_cmd
from shutil import copytree, copyfile, rmtree
import urllib
from helpers import Config, Helper, Logger
import subprocess


class WebTools:

    def __init__(self, dir="/var/www", cms="WORDPRESS", language="EN"):
        # If no root privileges, then quit, as not all tasks can be completed
        # without proper rights
        if geteuid() != 0:
            exit("You need to have root privileges to use this tool.")
        self.config = Config()
        if self.config.DB_USER == "" or self.config.DB_PASSWORD.strip() == "":
            exit("DB_USER and DB_PASSWORD are not properly configured. \
#Please edit in")
        self.logger = Logger()
        self.helper = Helper(self.logger)
        # get working dir
        proc = subprocess.Popen(["pwd"],
                                stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()
        self.pwd = out.strip()
        self.debug_is_on = False
        self.setDir(dir)
        self.setCMS(cms)
        self.setLanguage(language)
        self.folder_name = None
        self.enableJson(True)
        self.db_name = None
        self.db_user = None
        self.db_pass = None
        self.site_title = None
        self.site_admin_username = None
        self.site_admin_password = None
        self.site_admin_email = None
        self.site_blog_public = 0
        self.sites_available_dir = "/etc/apache2/sites-available/"
        self.sites_enabled_dir = "/etc/apache2/sites-enabled/"
        if str(os_name) == "posix":
            self.slash = "/"
        else:
            self.slash = "\\"

    def setDir(self, dir):
        self.dir = dir

    def setCMS(self, cms):
        # TODO: expand with other CMS
        if cms != "WORDPRESS":
            exit("For now, only Wordpress is supported!")
        self.cms = cms

    def setLanguage(self, language):
        if language in self.config.AVAILABLE_LANGUAGES:
            self.language = language
        else:
            raise AssertionError("Language '" + language + "' is not in list \
of valid languages: " + str(self.config.AVAILABLE_LANGUAGES))

    def setFolderName(self, name):
        self.logger.debug("***** Changing folder name")
        if (' ' in name) is True:
            self.logger.debug("   Could not set folder name to '" + name + "'\
, Spaces are not allowed!")
            return False
        if name not in self.getAllFolders():
            self.folder_name = name
            self.logger.debug("   Changed to: " + name)
            return True
        self.logger.debug("   Could not set folder name to '" + name + "', \
because it already exist!")
        return False

    def setSiteTitle(self, title):
        self.site_title = title

    def setSiteAdmin(self, username, password, email):
        self.site_admin_username = username
        self.site_admin_password = password
        self.site_admin_email = email

    def setBlogPublic(self, public):
        self.site_blog_public = public

    def enableJson(self, enable):
        self.return_as_json = enable

    def getAllFolders(self):
        self.logger.debug("***** get all folders in " + self.dir)
        folders = []
        # get a list with all folders
        for (dirpath, dirnames, filenames) in walk(self.dir):
            folders.extend(dirnames)
            break
        self.logger.debug("   -> folders: " + str(folders))
        return folders

    def getAllInstallations(self):
        self.logger.debug("***** looking for " + self.cms + " installations \
in " + self.dir)
        folders = self.getAllFolders()
        # check which folders that contain a CMS installation
        installations = []
        for folder in folders:
            path = self.dir + self.slash + folder
            self.logger.debug("   -> checking next folder: " + path)
            for subdir in listdir(path):
                folder_path = join(path, subdir)
                if isdir(folder_path) and subdir in \
                        self.config.CMS[self.cms]["dir_check"]:
                    self.logger.debug(
                        "      -> " + folder + " matches. ADDED!")
                    next = {"name": folder}
                    installations.append(next)
        if self.return_as_json:
            return self.as_json(installations)
        return installations

    def getSingle(self, domain):
        """
        Returns info about a single installation
        """
        path = self.dir + self.slash + domain + self.slash + 'wp-config.php'
        db_name = self.findInConfig(path, "DB_NAME")[0:16]
        db_user = self.findInConfig(path, "DB_USER")
        db_pass = self.findInConfig(path, "DB_PASSWORD")
        enabled = self.isEnabled(domain)
        return self.as_json({"db_name": db_name,
                             "db_user": db_user,
                             "db_pass": db_pass,
                             "enabled": enabled})

    def isEnabled(self, domain):
        """
        Check if a domain is active or not, based on whether is
        in sites-enabled.
        """
        return str(exists(self.sites_enabled_dir + domain + ".conf"))

    def findInConfig(self, filepath, item):
        # Open file
        result = "unknown"
        try:
            f = open(filepath, 'r')
            results = re.findall(r'define\(\'' + item + '\',\ \'(.+)\'\)',
                                 f.read())
            f.close()
            result = results[0]
        except:
            # TODO: better handling...
            pass
        return result

    def install(self):
        """
        1. download (wordpress).zip
        2. unpack
        3. move to correct location, and rename
        4. create database
        5. Edit config file with username, password etc
        6. Install wordpress
        """
        self.logger.info("***** Installing " + self.cms)
        is_valid, reason = self.isInfoValid()
        if is_valid:
            self.logger.info("1. download (wordpress).zip")
            file = self.config.CMS[self.cms]["download_url"].split("/")[-1]
            urllib.urlretrieve(self.config.CMS[self.cms]["download_url"], file)
            self.logger.info("2. unpack")
            tar = tarfile.open(file, 'r:gz')
            for item in tar:
                tar.extract(item)
            self.logger.info("3. move to correct location, and rename")
            copytree("wordpress", self.dir + self.slash + self.folder_name)
            # TODO: Remove files
            self.logger.info("4. create database and user")
            db_name, db_user, db_pass = self.setup_db()
            self.logger.info("5. Edit config file with username, password etc")
            self.create_config(db_name, db_user, db_pass)
            self.configure_apache()
            # change permissions
            system_cmd("chown -R www-data:www-data " +
                       self.dir + self.slash + self.folder_name)
            self.install_wordpress()
            self.enableSite(self.folder_name, "True")
            return self.as_json({"db_name": db_name,
                                 "db_user": db_user,
                                 "db_pass": db_pass})
        else:
            self.logger.info("Info could not be verified! Reason: " + reason)

    def configure_apache(self):
        """
        1. Copy and rename template to sites-available directory
        2. Edit configuration file to fit installation
        3. Enable site
        """
        new_file_path = self.sites_available_dir + self.folder_name + ".conf"
        copyfile(self.pwd + self.slash + "templates/template_sites-available",
                 new_file_path)
        alias = self.folder_name.replace("www.", "")
        self.helper.find_replace(new_file_path, "###SERVER_NAME###",
                                 self.folder_name)
        self.helper.find_replace(new_file_path, "###SERVER_ALIAS###",
                                 alias)
        self.helper.find_replace(new_file_path, "###SERVER_ADMIN###",
                                 self.site_admin_email)

    def enableSite(self, domain, enable):
        if enable.lower() == "true":
            try:
                system_cmd("a2ensite " + domain)
            except:
                # TODO: better handling
                pass
        else:
            try:
                system_cmd("a2dissite " + domain)
            except:
                # TODO: better handling
                pass
        # restart apache
        system_cmd("/etc/init.d/apache2 restart")

    def install_wordpress(self):
        """
        Install wordpress by calling install.php with required params
        """
        import urllib
        import urllib2
        import cookielib
        cj = cookielib.CookieJar()
        opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
        urllib2.install_opener(opener)
        login_data = urllib.urlencode({
            'weblog_title': self.site_title,
            'user_name': self.site_admin_username,
            'admin_password': self.site_admin_password,
            'admin_password2': self.site_admin_password,
            'admin_email': self.site_admin_email,
            'blog_public': self.site_blog_public
            })
        opener.open(
            "http://localhost/sites/" + self.folder_name +
            # step 2 is the step where form data is evaluated
            # and site is installed
            "/wp-admin/install.php?step=2",
            login_data)

    def sql_string(self, string):
        return string.replace(".", "_")

    def setup_db(self):
        """
        1. Create database
        2. Add user with privileges
        """
        self.logger.debug("***** Setting up the database.")
        if self.db_name is None:
            db_name = "orbit_" + self.sql_string(self.folder_name)
        else:
            db_name = self.sql_string(self.db_name)
        if self.db_user is None:
            # Use folder_name to avoid having the same user name twice
            # Note: must be no longer than 16 characters
            db_user = "user_" + self.sql_string(self.folder_name)[0:11]
        else:
            db_user = self.sql_string(self.db_user)
        if self.db_pass is None:
            # Generate a random password, if not set
            db_pass = self.helper.getRandomString()
        else:
            db_pass = self.db_pass
        # create database
        command = "mysql -u" + self.config.DB_USER + " --password=\
" + self.config.DB_PASSWORD.strip() + " -e \"create database " + db_name + "\""
        self.logger.debug("1. Create database")
        system_cmd(command)
        self.logger.debug("2. Add user with privileges")
        command = "mysql -uroot --password=" + self.config.DB_PASSWORD.strip() + " \
-e \"GRANT USAGE ON *.* TO " + db_user + "@localhost IDENTIFIED BY \
'" + db_pass + "'; GRANT ALL PRIVILEGES ON " + db_name + ".* TO \
" + db_user + "@localhost; FLUSH PRIVILEGES;\""
        system_cmd(command)
        return db_name, db_user, db_pass

    def create_config(self, db_name, db_user, db_pass):
        """
        1. Create a copy of sample config file
        2. Edit
        2.1 Edit db name
        2.2 Edit db username
        2.3 Edit db password
        """
        path = self.dir + self.slash + self.folder_name + self.slash
        self.helper.copy(
            path + self.config.CMS[self.cms]["config_sample_file_name"],
            path + self.config.CMS[self.cms]["config_file_name"]
            )
        # Set database name
        self.helper.find_replace(
            self.dir + self.slash + self.folder_name + self.slash +
            self.config.CMS[self.cms]["config_file_name"],
            self.config.CMS[self.cms]["config_default_db_name"],
            db_name)
        # Set database name
        self.helper.find_replace(
            self.dir + self.slash + self.folder_name + self.slash +
            self.config.CMS[self.cms]["config_file_name"],
            self.config.CMS[self.cms]["config_default_db_user"],
            db_user)
        # Set database name
        self.helper.find_replace(
            self.dir + self.slash + self.folder_name + self.slash +
            self.config.CMS[self.cms]["config_file_name"],
            self.config.CMS[self.cms]["config_default_db_pass"],
            db_pass)

    def isInfoValid(self):
        if self.dir is None:
            return False, "Directory not set!"
        elif self.cms is None:
            return False, "CMS not set!"
        elif self.language is None:
            return False, "Language not set!"
        elif not isdir(self.dir):
            return False, self.dir + " is not a valid directory!"
        elif self.folder_name is None:
            return False, "Folder name is not set!"
        elif self.folder_name in self.config.DISALLOWED_FOLDER_NAMES:
            return False, "Folder name is not allowed!"
        elif self.site_title is None:
            return False, "Site title is not set!"
        elif self.site_admin_username is None:
            return False, "Admin username is not set!"
        elif self.site_admin_password is None:
            return False, "Admin password is not set!"
        elif self.site_blog_public is None:
            return False, "Blog publicity is not set!"
        return True, ""

    def enableDebug(self, enable):
        self.logger.debug_is_on = enable

    def removeInstallation(self, domain):
        """
        1. Delete database and database privileges
        2. Delete files
        3. Disable in sites-enabled
        4. Delete configuration file
        """
        path = self.dir + self.slash + domain + self.slash + 'wp-config.php'
        db_name = self.findInConfig(path, "DB_NAME")
        db_name = "orbitweb"  # TEST
        db_user = self.findInConfig(path, "DB_USER")
        db_user = "testuser"  # TEST
        # remove database
        system_cmd("mysql -u " + self.config.DB_USER +
                   " -p" + self.config.DB_PASSWORD.strip() +
                   " -e \"DROP DATABASE IF EXISTS " + db_name + ";\"")
        # remove user
        system_cmd(
            "mysql -u " + self.config.DB_USER +
            " -p" + self.config.DB_PASSWORD.strip() +
            " -e \"DELETE FROM mysql.user where user='" +
            db_user + "'; FLUSH PRIVILEGES;\"")
        # Remove site from sites-enabled
        self.enableSite(domain, "False")
        # Remove configuration file
        remove(self.sites_available_dir + domain + ".conf")
        # remove directory
        try:
            rmtree(self.dir + domain)
        except:
            # TODO: better handling
            pass

    def as_json(self, input):
        return json.dumps(input)

if __name__ == "__main__":
    web = WebTools()
    #web.setDir("/var/www/orbit")
    #web.enableDebug(True)
    #web.setCMS("WORDPRESS")
    #web.enableJson(True)
    #web.setFolderName("www.example.com")
    #web.install()
