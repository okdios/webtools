"""

  Title        : Orbit WEB Access helper functions
  Description  : Helper classes for Orbit WEB Access Web Tools
  Author       : okdios
  Date         : 30/10-2013
  Version      : 0.1
  Python_ver.  : 2.7
  Notes        :

"""

from os import remove, close
from shutil import copy, move
from tempfile import mkstemp
import random
import string


class Config:

    def __init__(self):
        # For testing only...
        # Get password from file
        if self.DB_PASSWORD == "":
            try:
                with open("/var/.mysqlpass") as f:
                    self.DB_PASSWORD = f.read()
                f.close()
            except:
                pass

    # main configuration for different CMS
    CMS = {
        "WORDPRESS": {
            "dir_check": ["wp-content", "Scripts"],
            "download_url": "http://wordpress.org/latest.tar.gz",
            "config_sample_file_name": "wp-config-sample.php",
            "config_file_name": "wp-config.php",
            "config_default_db_name": "database_name_here",
            "config_default_db_user": "username_here",
            "config_default_db_pass": "password_here",
        },
    }

    # The default language for new installations, if no other language is set
    DEFAULT_LANG = "EN"

    # Available languages for new CMS installations
    AVAILABLE_LANGUAGES = ["EN", "DA"]

    # Folder names that we do not want new installations to have
    DISALLOWED_FOLDER_NAMES = ["wordpress", "www"]

    # Database credentials
    DB_USER = "root"
    DB_PASSWORD = ""


class Logger:
    def __init__(self):
        self.debug_is_on = False

    def setDebug(self, enable=True):
        self.debug_is_on = enable

    def info(self, text):
        print "(info)  " + text

    def debug(self, text):
        if self.debug_is_on:
            print "(debug) " + text


class Helper:

    def __init__(self, logger):
        self.logger = logger

    def getRandomString(self, size=15,
                        chars=string.ascii_uppercase +
                        string.ascii_lowercase + string.digits):
        return ''.join(random.choice(chars) for x in range(size))

    def find_replace(self, file_path, pattern, subst):
        #Create temp file
        fh, abs_path = mkstemp()
        new_file = open(abs_path, 'w')
        old_file = open(file_path)
        for line in old_file:
            new_file.write(line.replace(pattern, subst))
        #close temp file
        new_file.close()
        close(fh)
        old_file.close()
        #Remove original file
        remove(file_path)
        #Move new file
        move(abs_path, file_path)

    def copy(self, src, dst):
        self.logger.debug("Trying to copy '" + src + "'' to '" + dst + "'")
        try:
            copy(src, dst)
        except:
            # TODO: Better handling here...
            self.logger.debug("Copy FAILED!")
            raise
